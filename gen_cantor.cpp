#include<iostream>
#include<fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <vector>
#include <time.h>

using namespace std;

double RAND();

int main(int argc,char * const argv[])
{
  
  if ( argc != 5 )
    {
      cout << "please enter number of points, Hausdorff dimension ( 0 < d < 3 ),"
	" number of levels,  as well as output file address"
	"(e.g., 512  2.3 3  data.dat)" << endl;
      exit(1);
    }
  
  int N = atoi(argv[1]);
  double d = atof(argv[2]);  
  int l = atoi(argv[3]);

  ofstream output( argv[4] );
  // Find gamma in terms of Hausdorff dimension
  double gamma = 1 - 2 * exp( -3 * log(2)/d );
  
   cout << "Number of points = " << N << "  , d = " << d <<
    " , and gamma = " << gamma << endl
       << "Note: to have exactly one point per interval at level l,"
    " you should pick N = 8^l." << endl;
  
  /****************************************************/
  /*        GENERATING A 1D GENERAL CANTOR SET        */
  /****************************************************/
  
  vector<double> X1;
  vector<double> X2;
  
  X1.push_back(0);
  for ( int i(1); i <= l; i++ )
    {
      for ( int j(0); j < X1.size(); j++ )
	{
	  X2.push_back( X1[j] );
	  X2.push_back( X1[j] + pow((1-gamma)/2., i-1) * (1.+gamma)/2. );
	}
      X1 = X2;
      X2.clear();
    }
  
  /****************************************************/
  /*         GENERATING 3D TENSOR PRODUCT SET         */
  /****************************************************/
  
  output << N << endl; //first line of output file is number of points
  output << d << endl; //second line is Hausdorff dimension
  
  double distance = pow( (1-gamma)/2., l );
  double x,y,z;
  int n = X1.size(); // = 2^l
  int count = 0;

  for ( int i(0); i < n; i = (i+1)%n )
    {
      for ( int j(0); j < n; j++  )
	{
	  for ( int k(0); k < n; k++  )
	    {
	      // Pick a random point at level l of the Cantor set
	      x = ( X1[i] + RAND()*distance );
	      y = ( X1[j] + RAND()*distance );
	      z = ( X1[k] + RAND()*distance );
	      // Scale the point from [0 1] to [-1 1]
	      x = 2*x-1;
	      y = 2*y-1;
	      z = 2*z-1;
	      // Store the point into the output file
	      output << x << " " << y << " " << z << " " << 1 << endl;	      
	      if ( ( ++count ) == N ) goto stop;
	    }
	}
    }
 stop:
  output.close();
}

// Generate a random number between 0 and 1
double RAND()
{
  return ( (double) rand() ) / ( (double) RAND_MAX );
}
