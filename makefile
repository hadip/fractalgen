OBJS = fmm.o node.o position.o triple.o routines.o chebyshev.o precomputation.o kernel.o
CPP = g++
CFLAGS = -O3

default: gen_cantor

gen_cantor: gen_cantor.cpp
	$(CPP) -o gen_cantor gen_cantor.cpp $(CFLAGS)

clean:
	rm -rf gen_cantor *.o
